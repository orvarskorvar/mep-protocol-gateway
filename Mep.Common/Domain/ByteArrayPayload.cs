﻿using System.Linq;
using DotNetty.Buffers;
using Mep.Common.Utils;

namespace Mep.Common.Domain
{
    public class ByteArrayPayload : IPayload
    {
        private readonly byte[] _payLoadBytes;

        public ByteArrayPayload(IByteBuffer buffer)
        {
            var payloadLength = buffer.ReadableBytes;
            var payload = new byte[payloadLength];
            buffer.ReadSlice(payloadLength).GetBytes(0, payload);
            _payLoadBytes = payload;
        }

        public ByteArrayPayload(string payload)
        {
            _payLoadBytes = payload.AsByteArray();
        }

        public ByteArrayPayload(byte[] bytes) => _payLoadBytes = bytes;

        public IByteBuffer AsByteBuffer()
        {
            return Unpooled.CopiedBuffer(_payLoadBytes);
        }

        public IPayload FromByteBuffer(IByteBuffer buffer)
        {
            return new ByteArrayPayload(buffer);
        }

        public byte[] AsByteArray()
        {
            return _payLoadBytes;
        }

        public int GetPayLoadLength()
        {
            return _payLoadBytes.Length;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var pl = (ByteArrayPayload) obj;
            return _payLoadBytes.SequenceEqual(pl._payLoadBytes);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 27;
                hash = (13 * hash) + _payLoadBytes.GetHashCode();
                return hash;
            } 
        }
    }
}
