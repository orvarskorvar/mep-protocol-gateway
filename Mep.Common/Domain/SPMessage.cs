﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Mep.Common.Domain
{
    public class SpMessage
    {
        [XmlAttribute(AttributeName = "type")]
        public SpMessageType Type;
        [XmlAttribute(AttributeName = "status")]
        public string Status;
        [XmlAttribute(AttributeName = "work_pending")]
        public bool WorkPending;
        [XmlAttribute(AttributeName = "delay")]
        public int Delay;

        public Reading R;

        public enum SpMessageType 
        {
            set,
            get,
            register,
            result,
            reading,
            getWork,
            status
        }

        public SpMessage()
        {
            
        }

        public SpMessage(SpMessageType type, string status = "", bool workPending = false, int delay = 0, Reading r = null)
        {
            Type = type;
            Status = status;
            WorkPending = workPending;
            Delay = delay;
            R = r;
        }

        public SpMessage(string xml)
        {     
            var message = XElement.Parse(xml);
            Type = (SpMessageType) Enum.Parse(typeof(SpMessageType), message.Attribute("type")?.Value ?? throw new InvalidOperationException());
            Status = message.Attribute("status")?.Value;
            WorkPending = message.Attribute("work_pending")?.Value == "true";
            Delay = message.Attribute("delay")?.Value != null ? int.Parse(message.Attribute("delay")?.Value) : 0;
        }

        public static SpMessage Deserialize(string xml)
        {
            var serializer = new XmlSerializer(typeof(SpMessage));
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            return (SpMessage)serializer.Deserialize(memStream);
        }

        // override StringWriter
        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }

        private string GenerateXmlResponse(Object obj)
        {
            Type t = obj.GetType();

            var xml = "";

            using (StringWriter sww = new Utf8StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    var ns = new XmlSerializerNamespaces();
                    // add empty namespace
                    ns.Add("", "");
                    XmlSerializer xsSubmit = new XmlSerializer(t);
                    xsSubmit.Serialize(writer, obj, ns);
                    xml = sww.ToString(); // Your XML
                }
            }
            return xml;
        }

        public string ToXml()
        {

            var doc = new XDocument(
                new XDeclaration("1.0", "UTF-8", ""),
                new XElement("message", string.Empty,
                new XAttribute("type", Type.ToString().ToLower()),
                Status != "" ? new XAttribute("status", Status) : null, 
                WorkPending ? new XAttribute("work_pending", WorkPending) : null,
                Delay != 0 ? new XAttribute("delay", Delay) : null,

                R?.ToXNode()));

            var sw = new Utf8StringWriter();
            doc.Save(sw, SaveOptions.DisableFormatting);
            return sw.ToString();
        }

        public class Reading
        {
            private readonly List<R> _readings;

            public Reading() { }

            public Reading(List<R> readings)
            {
                this._readings = readings;
            }

            public XNode ToXNode()
            {
                return new XElement("readings",
                    from el in _readings
                    select el.ToXNode());
            }

            public class R
            {
                public int Value;
                public int Duration;
                public int Timestamp;

                public R() { }
                public R(int value, int duration, int timestamp = 0)
                {
                    Value = value;
                    Duration = duration;
                    Timestamp = timestamp;
                }

                public XNode ToXNode()
                {
                    return new XElement("r", new XAttribute("v", Value), new XAttribute("d", Duration),
                        Timestamp != 0 ? new XAttribute("t", Timestamp) : null);
                }
            }
        }

    }
}
