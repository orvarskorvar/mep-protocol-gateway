﻿using System;
using System.Collections;
using DotNetty.Buffers;
using DotNetty.Codecs;
using Mep.Common.Utils;

namespace Mep.Common.Domain
{
    public class HandShakePayLoad :IPayload
    {
        public ConnectionIdType CTypeId { get; }
        public ConnectionType CType { get; }
        public VarLong ConnectionId { get; }
        public VarLong SoftwareId { get; }
        public VarInt MajorVersion { get; }
        public VarInt MinorVersion { get; }
        public VarInt BuildNumber { get; }

        private readonly int _payLoadLength;

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var pl = (HandShakePayLoad)obj;

            if (CTypeId != pl.CTypeId)
                return false;
            if (CType != pl.CType)
                return false;
            if (ConnectionId != pl.ConnectionId)
                return false;
            if (SoftwareId != pl.SoftwareId)
                return false;
            if (MajorVersion != pl.MajorVersion)
                return false;
            if (MinorVersion != pl.MinorVersion)
                return false;
            if (BuildNumber != pl.BuildNumber)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 27;
                hash = (13 * hash) + CTypeId.GetHashCode();
                hash = (13 * hash) + CType.GetHashCode();
                hash = (13 * hash) + ConnectionId.GetHashCode();
                hash = (13 * hash) + SoftwareId.GetHashCode();
                hash = (13 * hash) + MajorVersion.GetHashCode();
                hash = (13 * hash) + MinorVersion.GetHashCode();
                hash = (13 * hash) + BuildNumber.GetHashCode();
                return hash;
            }
        }


        public HandShakePayLoad(int ctypeid = 1, int ctype = 1, long connectionId = 123456789, long softwareId = 9876543210, int majorVersion = 0, int minorVersion = 0, int buildNumber = 3333)
        {
            CTypeId = (ConnectionIdType)ctypeid;
            CType = (ConnectionType)ctype;
            ConnectionId = new VarLong(connectionId);
            SoftwareId = new VarLong(softwareId);
            MajorVersion = new VarInt(majorVersion);
            MinorVersion = new VarInt(minorVersion); 
            BuildNumber = new VarInt(buildNumber);
            _payLoadLength = AsByteBuffer().ReadableBytes;
        }

        public HandShakePayLoad(IByteBuffer buffer)
        {
            int start = buffer.ReaderIndex;
            // first byte is the CONTYPE
            var first = buffer.ReadByte();
            var firstBits = new BitArray(new[] { first });

            CTypeId = (ConnectionIdType) new BitArray(new[]
                {firstBits.Get(0), firstBits.Get(1), firstBits.Get(2), firstBits.Get(3)}).ToByte();

            CType = (ConnectionType) new BitArray(new[]
                {firstBits.Get(4), firstBits.Get(5), firstBits.Get(6), firstBits.Get(7)}).ToByte();

            ConnectionId = new VarLong(buffer);
            SoftwareId = new VarLong(buffer);
            MajorVersion = new VarInt(buffer);
            MinorVersion = new VarInt(buffer);
            BuildNumber = new VarInt(buffer);
            _payLoadLength = buffer.ReaderIndex - start;

            if (!buffer.IsReadable())
                return;
            var message = "Handshake Payload buffer should now be empty, but contains :" +
                          ByteBufferUtil.PrettyHexDump(buffer);
            Console.WriteLine(message);
            throw new EncoderException(message);
        }

        public IByteBuffer AsByteBuffer()
        {
            var buf = Unpooled.Buffer();
            var ctypeId = new BitArray(new[] { (byte)CTypeId });
            var ctype = new BitArray(new[] { (byte)CType });
            var first = new BitArray(new[]
            {
                    ctypeId.Get(0), ctypeId.Get(1), ctypeId.Get(2), ctypeId.Get(3), ctype.Get(0), ctype.Get(1),
                    ctype.Get(1), ctype.Get(3)
            }).ToByte();
            buf.WriteByte(first);
            buf.WriteBytes(ConnectionId.ToVliBuffer());
            buf.WriteBytes(SoftwareId.ToVliBuffer());
            buf.WriteBytes(MajorVersion.ToVliBuffer());
            buf.WriteBytes(MinorVersion.ToVliBuffer());
            buf.WriteBytes(BuildNumber.ToVliBuffer());
            return buf;
        }

        public IPayload FromByteBuffer(IByteBuffer buffer)
        {
            return new HandShakePayLoad(buffer);
        }

        public byte[] AsByteArray()
        {
            return AsByteBuffer().ToArray();
        }

        public int GetPayLoadLength()
        {
            return _payLoadLength;
        }

        public enum ConnectionIdType : byte
        {
            Gwid = 0x01,
            Mac = 0x02
        }

        public enum ConnectionType : byte
        {
            EthernetCable = 0x01,
            Sim12G = 0x02,
            Sim13G = 0x03,
            Sim22G = 0x05,
            Sim23G = 0x06,
            EthernetWifi = 0x08
        }

    }

}
