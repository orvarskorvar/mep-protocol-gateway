﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetty.Buffers;

namespace Mep.Common.Domain
{
    public static class EnvelopeHelper
    {
        public static Envelope InternalRequest(Envelope envelope)
        {
            if (envelope.Type != Envelope.EnvelopeType.Handshake)
                throw new ArgumentException("To create an InternalRequest you need to supply a HandshakeEnvelope not a :" + envelope.Type);

            var handshakePayload = (HandShakePayLoad) envelope.Payload;
            var internalRequestPayload = new InternalRequestPayload(handshakePayload);
            return new Envelope
            {
                Type = Envelope.EnvelopeType.InternalRequest,
                TcCrc = false,
                PacketId = 65,
                Payload = internalRequestPayload,
                PayloadLength = internalRequestPayload.GetPayLoadLength()
            };
        }

        public static Envelope InternalResponse(Envelope envelope)
        {
            if (envelope.Type != Envelope.EnvelopeType.Request)
                throw new ArgumentException("To create an InternalResponse you need to supply an InternalRequest not a :" + envelope.Type);

            var internalResponsePayload = new InternalResponsePayload();
            return new Envelope
            {
                Type = Envelope.EnvelopeType.InternalResponse,
                TcCrc = false,
                PacketId = 65,
                Payload = internalResponsePayload,
                PayloadLength = internalResponsePayload.GetPayLoadLength()
            };
        }

        public static Envelope Response(int pid, bool sucess, string payload)
        {
            var pload = new ByteArrayPayload(payload);
            var plen = pload.GetPayLoadLength();
            return new Envelope
            {
                Type = Envelope.EnvelopeType.Response,
                TcReq = sucess,
                PacketId = (byte) pid,
                Payload = pload,
                PayloadLength = plen
            };
        }

        public static Envelope Request(int pid, bool crc = false, bool comp = false, IPayload payload = null, int plen = 0)
        {
            return new Envelope
            {
                Type = Envelope.EnvelopeType.Request,
                TcCrc = crc,
                TcComp = comp,
                PacketId = (byte)pid,
                Payload = payload,
                PayloadLength = plen
            };
        }
        public static Envelope Post(bool crc, bool ack, int pid, IPayload payload = null, int plen = 0)
        {
            return new Envelope
            {
                Type = Envelope.EnvelopeType.Post,
                TcCrc = crc,
                TcAck = ack,
                PacketId = (byte)pid,
                Payload = payload,
                PayloadLength = plen
            };
        }

        public static Envelope Ack(int pid, bool status)
        {
            return new Envelope
            {
                Type = Envelope.EnvelopeType.Ack,
                TcStat = status,
                PacketId = (byte)pid
            };
        }
        public static Envelope KeepAlive(int pid, bool ack)
        {
            return new Envelope
            {
                Type = Envelope.EnvelopeType.Keepalive,
                TcAck = ack,
                PacketId = (byte)pid
            };
        }
    }
}
