﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mep.Common.Domain
{
    // Class that keeps track of global connection stuff..

    class ConnectionState
    {
        private int Pid { get; set; }

        public int GetNextClientPid()
        {
            Pid++;
            if (Pid > 60)
                Pid = 1;
            return Pid;
        }

        public int GetNextServerPid()
        {
            Pid++;
            if (Pid < 61 || Pid > 120)
                Pid = 61;
            return Pid;
        }
    }
}
