﻿using System;
using System.Collections;
using DotNetty.Buffers;
using DotNetty.Codecs;
using Mep.Common.Utils;
using NUnit.Framework;

namespace Mep.Common.Domain
{
    public class Envelope
    {
        public EnvelopeType Type { get; set; }
        public bool TcCrc { get; set; }
        public bool TcAck { get; set; }
        public bool TcComp { get; set; }
        public bool TcReq { get; set; }
        public bool TcStat { get; set; }

        public ProtocolVersion PVersion { get; }
        public ProtocolType PType { get; }
        public HandshakeType HType { get; }
        public byte PacketId { get; set; }
        public int PayloadLength { get; set; }
        public IPayload Payload { get; set; }
        public ushort Checksum { get; }

        public enum EnvelopeType : byte
        {
            Handshake = 0x01,
            Request = 0x02,
            Response = 0x03,
            Post = 0x04,
            Keepalive = 0x06,
            Ack = 0x07,
            InternalRequest = 0x08,
            InternalResponse = 0x09
        }

        public enum ProtocolVersion : byte
        {
            V10 = 0x01
        }

        public enum ProtocolType : byte
        {
            GoogleProtobuf = 0x01,
            Json = 0x02,
            Xml = 0x03
        }

        public enum HandshakeType : byte
        {
            Simple = 0x01,
            AesChallenge = 0x02
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var env = (Envelope) obj;

            if (Type != env.Type)
                return false;
            if (TcCrc != env.TcCrc)
                return false;
            if (TcAck != env.TcAck)
                return false;
            if (TcComp != env.TcComp)
                return false;
            if (TcReq != env.TcReq)
                return false;
            if (TcStat != env.TcStat)
                return false;

            if (PVersion != env.PVersion)
                return false;
            if (PType != env.PType)
                return false;
            if (HType != env.HType)
                return false;
            if (PacketId != env.PacketId)
                return false;
            if (PayloadLength != env.PayloadLength)
                return false;
            if (Payload != null && env.Payload != null)
            {
                if (!Payload.Equals(env.Payload))
                    return false;
            } else if (Payload != null || env.Payload != null)
            {
                return false;
            }

            return Checksum == env.Checksum;
        }

        public override int GetHashCode()
        {
            unchecked
            { 
                var hash = 27;
                hash = (13 * hash) + Type.GetHashCode();
                hash = (13 * hash) + TcCrc.GetHashCode();
                hash = (13 * hash) + TcAck.GetHashCode();
                hash = (13 * hash) + TcComp.GetHashCode();
                hash = (13 * hash) + TcReq.GetHashCode();
                hash = (13 * hash) + TcStat.GetHashCode();
                hash = (13 * hash) + PVersion.GetHashCode();
                hash = (13 * hash) + PType.GetHashCode();
                hash = (13 * hash) + HType.GetHashCode();
                hash = (13 * hash) + PacketId.GetHashCode();
                hash = (13 * hash) + PayloadLength.GetHashCode();
                hash = (13 * hash) + Payload.GetHashCode();
                hash = (13 * hash) + Checksum.GetHashCode();
                return hash;
            }
        }

        public Envelope(IByteBuffer envelope)
        {
            var fullEnvelope = Unpooled.CopiedBuffer(envelope);
            // first byte is the envelopeType
            var first = envelope.ReadByte();
            var firstBits = new BitArray(new[] { first });
            var type = ToByte(new BitArray(new bool[]
                {firstBits.Get(0), firstBits.Get(1), firstBits.Get(2), firstBits.Get(3)}));
            if (type == 0x00 || type == 0x05 || type > 0x09)
            {
                throw new EncoderException("Illegal envelope envelopeType: " + first);
            }
            Type = (EnvelopeType)type;
            // bit 4-7 are parameters.
            // bit 4 = CRC
            TcCrc = firstBits.Get(4);
            // bit 5 = ACK
            TcAck = firstBits.Get(5);
            // bit 7 = COMP
            TcComp = firstBits.Get(7);

            bool usePid = true;
            switch (Type)
            {
                case EnvelopeType.Handshake:
                    PVersion = (ProtocolVersion) envelope.ReadByte();
                    PType = (ProtocolType) envelope.ReadByte();
                    HType = (HandshakeType) envelope.ReadByte();
                    usePid = TcAck;
                    break;
                // Respons: bit 5 is REQUEST STATUS
                case EnvelopeType.Response:
                    TcReq = firstBits.Get(5);
                    TcAck = false;
                    break;
                // Ack: bit 7 is STATUS
                case EnvelopeType.Ack:
                    TcStat = firstBits.Get(7);
                    TcComp = false;
                    break;
                case EnvelopeType.Keepalive:
                    usePid = false;
                    break;
                case EnvelopeType.Request:
                    TcAck = false;
                    break;
                case EnvelopeType.Post:
                    usePid = TcAck;
                    break;
                case EnvelopeType.InternalRequest:
                case EnvelopeType.InternalResponse:
                    TcAck = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (usePid)
            {
                PacketId = envelope.ReadByte();
            }

            if (Type != EnvelopeType.Keepalive || Type != EnvelopeType.Ack)
            {        
                PayloadLength = Vli.DecodeVarInt(envelope);
                var payload = envelope.ReadSlice(PayloadLength);
                switch (Type)
                {
                    case EnvelopeType.Handshake:
                        Payload = new HandShakePayLoad(payload);
                        break;
                    case EnvelopeType.InternalRequest:
                        Payload = new InternalRequestPayload(payload);
                        break;
                    case EnvelopeType.Request:          
                    case EnvelopeType.Response:
                    case EnvelopeType.Post:
                    case EnvelopeType.Keepalive:
                    case EnvelopeType.Ack:
                    case EnvelopeType.InternalResponse:
                    default:
                        Payload = new ByteArrayPayload(payload);
                        break;
                }
                if (TcCrc)
                {
                    var content = fullEnvelope.ReadSlice(envelope.ReaderIndex);
                    Checksum = envelope.ReadUnsignedShort();
                    Crc16.CheckChecksum(content, Checksum);
                }

            }

            // envelope should now be empty.
            if (!envelope.IsReadable()) return;

            var message = "Envelope " + Type + " should be empty, but contains :\n" +
                             ByteBufferUtil.PrettyHexDump(envelope);
            Console.WriteLine(message);
            throw new EncoderException(message);
        }

        public Envelope(EnvelopeType type = EnvelopeType.Keepalive, bool tcCrc = false, bool tcAck = false, bool tcComp = false, bool tcReq = false, bool tcStat = false,
            int pVersion = 0, int pType = 0, int hType = 0, byte packetId = 0, int payloadLength = 0, IPayload payload = null, ushort checksum = 0)
        {
            Type = type;
            TcCrc = tcCrc;
            TcAck = tcAck;
            TcComp = tcComp;
            TcReq = tcReq;
            TcStat = tcStat;
            PVersion = (ProtocolVersion) pVersion;
            PType = (ProtocolType) pType;
            HType = (HandshakeType) hType;
            PacketId = packetId;
            Payload = payload;
            PayloadLength = 0;
            if (payload != null) PayloadLength = payload.GetPayLoadLength();
            Checksum = checksum;
        }

        public IByteBuffer AsByteBuffer()
        {
            var buffer = Unpooled.Buffer();

            var first = new BitArray(new[] { (byte) Type });
            
            first.Set(4, TcCrc);
            first.Set(5, TcAck);
            first.Set(7, TcComp);

            bool usePid = true;
            switch (Type)
            {
                case EnvelopeType.Handshake:
                    buffer.WriteByte(ToByte(first));
                    buffer.WriteByte((byte) PVersion);
                    buffer.WriteByte((byte) PType);
                    buffer.WriteByte((byte) HType);
                    usePid = TcAck;
                    break;
                case EnvelopeType.Response:
                    first.Set(5, TcReq);
                    break;
                case EnvelopeType.Keepalive:
                    first.Set(4, false);
                    first.Set(7, false);
                    usePid = TcAck;
                    break;
                case EnvelopeType.Ack:
                    first.Set(4, false);
                    first.Set(5, false);
                    first.Set(7, TcStat);
                    break;
                case EnvelopeType.Request:
                    break;
                case EnvelopeType.Post:
                    usePid = TcAck;
                    break;
                case EnvelopeType.InternalRequest:
                case EnvelopeType.InternalResponse:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            if (Type != EnvelopeType.Handshake)
                buffer.WriteByte(ToByte(first));

            if (usePid)
                buffer.WriteByte(PacketId);

            if (Type == EnvelopeType.Keepalive && Type == EnvelopeType.Ack) return buffer;
           
            buffer.WriteBytes(Vli.EncodeVariableLengthInt((ulong) PayloadLength));
            if (Payload != null) {
                buffer.WriteBytes(Payload.AsByteBuffer());
            }

            if (!TcCrc) return buffer;

            var content = Unpooled.CopiedBuffer(buffer);
            buffer.WriteUnsignedShort(Crc16.ComputeChecksum(content));
            return buffer;
        }

        private static byte ToByte(BitArray bits)
        {
            var bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        public static bool CheckEnvelope(Envelope envelope, ArrayList expected)
        {
            /*
             * expected = { EnvelopeType, TcCrc, TcAck, TcComp, TcReq, TcStat, TcAck,
             *              ProtocolVersion, ProtocolType, HandshakeType,
             *              PacketId, PayloadLength, Payload, Checksum }
             */

            if (expected[0] != null)
                Assert.AreEqual((EnvelopeType)expected[0], envelope.Type);
            if (expected[1] != null)
                Assert.AreEqual((bool)expected[1], envelope.TcCrc);
            if (expected[2] != null)
                Assert.AreEqual((bool)expected[2], envelope.TcAck);
            if (expected[3] != null)
                Assert.AreEqual((bool)expected[3], envelope.TcComp);
            if (expected[4] != null)
                Assert.AreEqual((bool)expected[4], envelope.TcReq);
            if (expected[5] != null)
                Assert.AreEqual((bool)expected[5], envelope.TcStat);
            if (expected[6] != null)
                Assert.AreEqual((bool)expected[6], envelope.TcAck);
            if (expected[7] != null)
                Assert.AreEqual((ProtocolVersion)expected[7], envelope.PVersion);
            if (expected[8] != null)
                Assert.AreEqual((ProtocolType)expected[8], envelope.PType);
            if (expected[9] != null)
                Assert.AreEqual((HandshakeType)expected[9], envelope.HType);
            if (expected[10] != null)
                Assert.AreEqual((byte)expected[10], envelope.PacketId);
            if (expected[11] != null)
                Assert.AreEqual((int)expected[11], envelope.PayloadLength);
            if (expected[12] != null)
                Assert.AreEqual((byte[])expected[12], envelope.Payload.AsByteArray());
            if (expected[13] != null)
                Assert.AreEqual((ushort)expected[13], envelope.Checksum);
            return true;
        }
    }
}
