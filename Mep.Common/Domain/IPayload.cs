﻿using DotNetty.Buffers;

namespace Mep.Common.Domain
{
    public interface IPayload
    {
        IByteBuffer AsByteBuffer();

        IPayload FromByteBuffer(IByteBuffer buffer);

        byte[] AsByteArray();

        int GetPayLoadLength();
    }
}
