﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mep.Common.Domain
{
    public class DpMessage
    {
        public DpMessageType Type;

        public enum DpMessageType
        {
            Telemetry,
            State,
            Properties,
            OtaReset,
            Diagnostics
        }

        public DpMessage(string json)
        {
            
        }

        public string ToJson()
        {
            return "";
        }

    }
}
