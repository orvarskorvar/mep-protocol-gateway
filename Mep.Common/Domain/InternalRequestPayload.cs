﻿using System;
using System.Linq;
using DotNetty.Buffers;
using Mep.Common.Utils;

namespace Mep.Common.Domain
{
    public class InternalRequestPayload : IPayload
    {
        private RequestType _requestType;
        private byte[] _challenge;
        private int PayLoadLength { get; }

        private VarInt TimeStamp { get; set; }

        public InternalRequestPayload(IByteBuffer buffer)
        {
            int start = buffer.ReaderIndex;
            _requestType = (RequestType)buffer.ReadByte();
            _challenge = buffer.ReadSlice(16).ToArray();
            TimeStamp = new VarInt(buffer);
            PayLoadLength = buffer.ReaderIndex - start;
        }

        public InternalRequestPayload(VarLong connectionId, VarInt timeStamp, VarInt majorVersion, VarInt minorVersion, VarInt buildNumber)
        {
            //var toEncrypt = connectionId.ToArray().Concat(timeStamp.ToArray()).Concat(majorVersion.ToArray()).Concat(minorVersion.ToArray()).Concat(buildNumber.ToArray()).ToArray();
            // AAAARRRGGGHH Det stämmer inte! Specen är FEL FEL FEL! Nä nu skiter jag i detta och bara skickar något. Det verkar ändå inte betyda något vad jag skickar..?
            _requestType = RequestType.ChallengeRequest;
            _challenge = ByteUtils.GetRandomArray(16);
            TimeStamp = timeStamp;
            PayLoadLength = 1 + 16 + TimeStamp.ToVliBuffer().ReadableBytes;
        }

        public InternalRequestPayload(HandShakePayLoad payload)
            : this(payload.ConnectionId, new VarInt((int)((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds()), payload.MajorVersion, payload.MinorVersion, payload.BuildNumber)
        {       
        }

        public IByteBuffer AsByteBuffer()
        {
            var buf = Unpooled.Buffer();
            buf.WriteByte((byte) _requestType);
            buf.WriteBytes(_challenge);
            buf.WriteBytes(TimeStamp.ToVliBuffer());
            return buf;
        }

        public IPayload FromByteBuffer(IByteBuffer buffer)
        {
            return new InternalRequestPayload(buffer);
        }

        public byte[] AsByteArray()
        {
            return AsByteBuffer().ToArray();
        }

        public int GetPayLoadLength()
        {
            return PayLoadLength;
        }

        private enum RequestType : byte
        {
            ChallengeRequest = 0x01
        }
    }
}
