﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetty.Buffers;
using Mep.Common.Utils;

namespace Mep.Common.Domain
{
    class InternalResponsePayload : IPayload
    {
        private byte[] _bytes;
        private int PayLoadLength = 16;
        public InternalResponsePayload(IByteBuffer buffer)
        {
        }
        public InternalResponsePayload()
        {
            _bytes = ByteUtils.GetRandomArray(16);
        }

        public IByteBuffer AsByteBuffer()
        {
            var buf = Unpooled.Buffer();
            buf.WriteBytes(_bytes);
            return buf;
        }

        public IPayload FromByteBuffer(IByteBuffer buffer)
        {
            throw new NotImplementedException();
        }

        public byte[] AsByteArray()
        {
            throw new NotImplementedException();
        }

        public int GetPayLoadLength()
        {
            return PayLoadLength;
        }
    }
}
