﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;

namespace Mep.Common.Handlers
{
    public class HdlcEncoder : MessageToByteEncoder<IByteBuffer>
    {
        private readonly byte _frameDelimiterByte = 0x7e; // ~
        private readonly byte _escapeByte = 0x7d; // }

        protected override void Encode(IChannelHandlerContext context, IByteBuffer message, IByteBuffer output)
        {
            output.WriteByte(_frameDelimiterByte);
            while (message.IsReadable())
            {
                var b = message.ReadByte();
                // escape bytes if needed
                if (b == _escapeByte || b == _frameDelimiterByte)
                {
                    output.WriteByte(_escapeByte);
                    output.WriteByte(b ^ 0x20);
                }
                else
                {
                    output.WriteByte(b);
                }
            }
            output.WriteByte(_frameDelimiterByte);
        }
    }
}
