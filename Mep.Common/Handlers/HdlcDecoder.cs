﻿using System;
using System.Collections.Generic;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Common.Utilities;
using DotNetty.Transport.Channels;

namespace Mep.Common.Handlers
{
    public class HdlcDecoder: ByteToMessageDecoder
    {
        private readonly byte _frameDelimiterByte = 0x7e;
        private readonly byte _escapeByte = 0x7d;
        //private bool _inFrame;
        //private bool _lastByteWasEscapeByte;
        //private IByteBuffer _frame;
        //public static readonly AttributeKey<String> id = AttributeKey<String>.NewInstance("ID");
        public static readonly AttributeKey<object> InFrameKey = AttributeKey<object>.NewInstance("inFrame");
        public static readonly AttributeKey<object> LastByteWasEscapeByteKey = AttributeKey<object>.NewInstance("lastByteWasEscapeByte");
        public static readonly AttributeKey<object> FrameKey = AttributeKey<object>.NewInstance("frame");

        public override bool IsSharable => true;
        public override void ChannelActive(IChannelHandlerContext context)
        {
            context.GetAttribute(InFrameKey).Set(false);
            context.GetAttribute(LastByteWasEscapeByteKey).Set(false);
            context.GetAttribute(FrameKey).Set(context.Allocator.Buffer());
            //_frame = context.Allocator.Buffer();
            base.ChannelActive(context);
        }

        protected override void Decode(IChannelHandlerContext context, IByteBuffer input, List<object> output)
        {
            if (input.ReadableBytes <= 0)
                return;

            byte currentByte = input.ReadByte();
            var inFrame = (bool) context.GetAttribute(InFrameKey).Get();
            if (inFrame)
            {
                var lastByteWasEscapeByte = (bool)context.GetAttribute(LastByteWasEscapeByteKey).Get();
                var frame = (IByteBuffer)context.GetAttribute(FrameKey).Get();
                if (lastByteWasEscapeByte)
                {
                    // escape byte and reverse bit 5 
                    frame.WriteByte(currentByte ^ 0x20);
                    context.GetAttribute(FrameKey).Set(frame);
                    context.GetAttribute(LastByteWasEscapeByteKey).Set(false);
                    //lastByteWasEscapeByte = false;
                    return;
                }
                if (currentByte == _frameDelimiterByte)
                {
                    //frame is done
                    output.Add(frame);
                    //inFrame = false;
                    context.GetAttribute(InFrameKey).Set(false);
                    context.GetAttribute(LastByteWasEscapeByteKey).Set(false);
                    //lastByteWasEscapeByte = false;
                    frame = context.Allocator.Buffer();
                    context.GetAttribute(FrameKey).Set(frame);
                    return;
                }
                if (currentByte == _escapeByte)
                {
                    context.GetAttribute(LastByteWasEscapeByteKey).Set(true);
                    //lastByteWasEscapeByte = true;
                    return;
                }
                frame.WriteByte(currentByte);
                context.GetAttribute(FrameKey).Set(frame);
                return;
            }
            if (currentByte != _frameDelimiterByte) throw new CorruptedFrameException("HDLC Frame is out of sync");
            //inFrame = true;
            context.GetAttribute(InFrameKey).Set(true);
        }
    }
}