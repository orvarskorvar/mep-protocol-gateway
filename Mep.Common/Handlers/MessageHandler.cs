﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DotNetty.Transport.Channels;
using Newtonsoft.Json.Linq;

namespace Mep.Common.Handlers
{
    class MessageHandler : ChannelHandlerAdapter
    {

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {

        }

        public string toJson()
        {

            var message = new JObject(
                new JProperty("message",
                    new JObject("content")));

            return message.ToString();
        }

        public string toXml()
        {
            XElement contacts =
                new XElement("Contacts",
                    new XElement("Contact",
                        new XElement("Name", "Patrick Hines"),
                        new XElement("Phone", "206-555-0144"),
                        new XElement("Address",
                            new XElement("Street1", "123 Main St"),
                            new XElement("City", "Mercer Island"),
                            new XElement("State", "WA"),
                            new XElement("Postal", "68042")
                        )
                    )
                );
            return contacts.ToString();
        }
    }

}
