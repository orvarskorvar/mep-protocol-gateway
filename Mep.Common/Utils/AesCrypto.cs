﻿using System.IO;
using System.Security.Cryptography;

namespace Mep.Common.Utils
{
    public static class AesCrypto
    {
        static byte[] _mepPrivateKey =
        {
            0xb0, 0x40, 0xa4, 0x40,
            0x07, 0x09, 0xf8, 0x50,
            0x10, 0x3f, 0x35, 0x20,
            0xa1, 0x0c, 0x30, 0xef,
            0x90, 0xf0, 0xc0, 0x07,
            0x0d, 0x13, 0x70, 0x30,
            0xd3, 0x4a, 0x6c, 0x9e,
            0xc7, 0x0b, 0xd4, 0x8a,
        };

        private static byte[] _iv;

       
        public static byte[] Encrypt(byte [] data)
        {
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = _mepPrivateKey;
                _iv = aesAlg.IV;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                return PerformCryptography(encryptor, data);
            }
        }

        public static byte[] Decrypt(byte[] data)
        {
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = _mepPrivateKey;
                aesAlg.IV = _iv;

                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                return PerformCryptography(decryptor, data);
            }

        }

        private static byte[] PerformCryptography(ICryptoTransform cryptoTransform, byte[] data)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(data, 0, data.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }

    }
}
