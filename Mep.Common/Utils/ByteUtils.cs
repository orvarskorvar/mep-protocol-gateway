﻿using System;
using System.Collections;
using System.IO;
using DotNetty.Buffers;

namespace Mep.Common.Utils
{
    public static class ByteUtils
    {
        public static byte ToByte(this BitArray bits)
        {
            var bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        
        public static byte[] Concat(this byte[] array1, byte[] array2)
        {
            var newSize = array1.Length + array2.Length;
            using (var ms = new MemoryStream(new byte[newSize], 0, newSize, true, true))
            {
                ms.Write(array1, 0, array1.Length);
                ms.Write(array2, 0, array2.Length);
                return ms.GetBuffer();
            }  
        }

        public static byte[] GetRandomArray(int length)
        {
            var random = new Random();
            var bytes = new byte[length];
            random.NextBytes(bytes);
            return bytes;
        }

        public static IByteBuffer GetAsBuffer(byte[] bytes)
        {
            var buf = Unpooled.Buffer();
            foreach (var b in bytes)
            {
                buf.WriteByte(b);
            }
            return buf;
        }

        public static IByteBuffer AsBuffer(this byte[] bytes)
        {
            return GetAsBuffer(bytes);
        }

        public static byte[] GetAsByteArray(string str)
        {
            var arr = new byte[str.Length];
            int i = 0;
            foreach (var b in str)
            {
                arr[i] = (byte)b;
                i++;
            }
            return arr;
        }

        public static byte[] AsByteArray(this string str)
        {
            return GetAsByteArray(str);
        }
    }
}
