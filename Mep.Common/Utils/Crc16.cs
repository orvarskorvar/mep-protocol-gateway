﻿using System;
using DotNetty.Buffers;

namespace Mep.Common.Utils
{
    public static class Crc16
    {
        private const ushort Polynomial = 0xA001;
        private static readonly ushort[] Table = new ushort[256];
        public static ushort ComputeChecksum(IByteBuffer content)
        {
            var buf = Unpooled.CopiedBuffer(content);
            ushort crc = 0;
            while (buf.IsReadable())
            {
                var t = buf.ReadByte();
                var index = (byte)(crc ^ t);
                crc = (ushort)((crc >> 8) ^ Table[index]);
            }
            return crc;
        }

        public static void CheckChecksum(IByteBuffer content, ushort checksum)
        {
            var computedChecksum = ComputeChecksum(content);
            if (computedChecksum != checksum)
            {
                throw new DataMisalignedException("Envelope checksum: " + checksum + " is not equal to computed checksum: " + computedChecksum);
            }

        }
        static Crc16()
        {
            for (ushort i = 0; i < Table.Length; ++i)
            {
                ushort value = 0;
                var temp = i;
                for (byte j = 0; j < 8; ++j)
                {
                    if (((value ^ temp) & 0x0001) != 0)
                    {
                        value = (ushort)((value >> 1) ^ Polynomial);
                    }
                    else
                    {
                        value >>= 1;
                    }
                    temp >>= 1;
                }
                Table[i] = value;
            }
        }
    }
}
