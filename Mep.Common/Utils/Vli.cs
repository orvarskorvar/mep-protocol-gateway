﻿using System;
using DotNetty.Buffers;

namespace Mep.Common.Utils
{
    public class VarInt
    {
        private readonly int _value;

        public VarInt(int value)
        {
            _value = value;
        }
        public VarInt(IByteBuffer buf)
        {
            _value = Vli.DecodeVarInt(buf);
        }

        public IByteBuffer ToVliBuffer()
        {
            return Vli.EncodeVarInt(_value);
        }

        public int Value()
        {
            return _value;
        }

        public byte[] ToArray()
        {
            return BitConverter.GetBytes(_value);
        }
    }

    public class VarLong
    {
        private readonly long _value;

        public VarLong(long value)
        {
            _value = value;
        }
        public VarLong(IByteBuffer buf)
        {
            _value = Vli.DecodeVarLong(buf);
        }

        public IByteBuffer ToVliBuffer()
        {
            return Vli.EncodeVarLong(_value);
        }
        public byte[] ToArray()
        {
            return BitConverter.GetBytes(_value);
        }

        public long Value()
        {
            return _value;
        }
    }
    public static class Vli
    {
        private static Int64 MacToInt64(string macAddress)
        {
            string hex = macAddress.Replace(":", "");
            return (long)Convert.ToUInt64(hex, 16);
        }

        private static string Int64ToMac(Int64 macAddress)
        {
            //TODO This might be needed if the iothub whants the mac not as an integer..
            return "";
        }

        public static ulong DecodeVariableLengthInt(IByteBuffer buffer)
        {
            ulong integer = 0;
            byte b;
            do
            {
                b = buffer.ReadByte();
                integer <<= 7;
                integer |= (b & 0x7fUL);
            } while ((b & 0x80) != 0);
            return integer;
        }


        public static uint ReadVariableLengthQuantity(IByteBuffer buf)
        {
            var index = 0;
            uint buffer = 0;
            byte current;
            do
            {
                if (index++ == 8)
                    throw new FormatException("Could not read variable-length quantity from provided stream.");

                buffer <<= 7;

                current = buf.ReadByte();
                buffer |= (current & 0x7FU);
            } while ((current & 0x80) != 0);

            return buffer;
        }

        public static int DecodeVarInt(IByteBuffer buf)
        {
            int value = 0, i = 0, b;
            while (((b = buf.ReadByte()) & 0x80) != 0)
            {
                value |= (b & 0x7F) << i;
                i += 7;
                CheckArguments(i <= 35, "Variable length integer is too long");
            }
            return value | (b << i);
        }

        public static IByteBuffer EncodeVarInt(int value)
        {
            var buffer = Unpooled.Buffer(5);           
            while ((value & 0xFFFFFF80) != 0L)
            {
                buffer.WriteByte((byte)(value & 0x7F) | 0x80);
                value >>= 7;
            }
            buffer.WriteByte((byte)value & 0x7F);         
            return buffer;
        }

        public static long DecodeVarLong(IByteBuffer buf)
        {
            long value = 0L, b;
            int i = 0;
            while (((b = buf.ReadByte()) & 0x80L) != 0)
            {
                value |= (b & 0x7F) << i;
                i += 7;
                CheckArguments(i <= 63, "Variable length long is too long");
            }
            return value | (b << i);
        }

        public static IByteBuffer EncodeVarLong(long val)
        {
            var value = (ulong) val;
            var buffer = Unpooled.Buffer(5);
            while ((value & 0xFFFFFFFFFFFFFF80L) != 0L)
            {
                buffer.WriteByte((byte)(((int)value & 0x7F) | 0x80));
                value >>= 7;
            }
            buffer.WriteByte((byte)value & 0x7F);
            return buffer;
        }

        private static void CheckArguments(bool value, string message)
        {
            if (!value)
                throw new ArgumentException(message);
        }

        public static IByteBuffer EncodeVariableLengthInt(ulong integer)
        {
            if (integer > Math.Pow(2, 56))
                throw new OverflowException("Integer exceeds max value.");
            var buffer = Unpooled.Buffer();
            if (integer == 0)
            {
                buffer.WriteByte(0);
                return buffer;
            }
            var index = 7;
            var significantBitReached = false;
            var mask = 0x7fUL << (index * 7);
            while (index >= 0)
            {
                var b = (mask & integer);
                if (b > 0 || significantBitReached)
                {
                    significantBitReached = true;
                    b >>= index * 7;
                    if (index > 0)
                        b |= 0x80;
                    buffer.WriteByte((byte)b);
                }
                mask >>= 7;
                index--;
            }
            return buffer;
        }



        public static IByteBuffer GetAsBuffer(byte[] bytes)
        {
            var buf = Unpooled.Buffer();
            foreach (var b in bytes)
            {
                buf.WriteByte(b);
            }
            return buf;
        }

        public static byte[] AsByteArray(this IByteBuffer buffer)
        {
            var buf = Unpooled.CopiedBuffer(buffer);
            var arr = new byte[buf.ReadableBytes];
            var i = 0;
            while (buf.IsReadable())
            {
                arr[i++] = buf.ReadByte();
            }
            return arr;
        }

        public static ulong VliBufferToInt(this IByteBuffer buffer)
        {
            return DecodeVariableLengthInt(buffer);
        }
 
        public static IByteBuffer ToVliBuffer(this ulong integer)
        {
            return EncodeVariableLengthInt(integer);
        }
    }
}
