﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging.Console;

using DotNetty.Handlers.Tls;
using DotNetty.Common.Internal.Logging;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Mep.Common.Handlers;

namespace Mep.Client
{
    internal class Client
    {
        private readonly string host;
        private readonly int port;
        private readonly int numberOfClients;

        public Client(string host, int port, int numberOfClients)
        {
            this.host = host;
            this.port = port;
            this.numberOfClients = numberOfClients;
        } 

        private async Task Start()
        {
            InternalLoggerFactory.DefaultFactory.AddProvider(new ConsoleLoggerProvider((s, level) => true, false));
            IEventLoopGroup group = new MultithreadEventLoopGroup();

            var certificate = new X509Certificate2(Path.Combine(AppContext.BaseDirectory, "mep-server.pfx"), "!password");
            var certHost = certificate.GetNameInfo(X509NameType.DnsName, false);
            var b = new Bootstrap();
            b.Group(group)
                .Channel<TcpSocketChannel>()
                .Option(ChannelOption.TcpNodelay, true)
                .Handler(new ActionChannelInitializer<ISocketChannel>(channel =>
                {
                    var pipeline = channel.Pipeline;
                    pipeline.AddLast("ssl", new TlsHandler(stream => new SslStream(stream, true, (sender, cert, chain, errors) => true), new ClientTlsSettings(certHost)));
                    pipeline.AddLast(new LoggingHandler());
                    pipeline.AddLast("hdlc-dec", new HdlcDecoder());
                    pipeline.AddLast("hdlc-enc", new HdlcEncoder());              
                    pipeline.AddLast("mep-client", new ClientHandler());
                }));

            var clientNumber = 0;
            var boundChannels = new List<IChannel>();
            while (numberOfClients > clientNumber)
            {             
                try
                {
                    Console.WriteLine("Client: " + clientNumber + " Connecting to: " + host + " on port: " + port);
                    var boundChannel = await b.ConnectAsync(new IPEndPoint(IPAddress.Parse(host), port));
                    boundChannels.Add(boundChannel);
                    Console.WriteLine("Client: " + clientNumber + " Connected.");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Client: " + clientNumber + " Could not connect to server, connection refused!" + ex.Message);
                }
                clientNumber++;
            }

            Console.WriteLine("All clients connected, press any key to disconnect and quit.");
            Console.ReadKey(false);
            foreach (var channel in boundChannels)
            {
                Console.WriteLine("Closing clients...");
                channel.CloseAsync().Wait();
            }
            group.ShutdownGracefullyAsync().Wait();
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("STARTING CLIENT");
            if (args.Length != 3)
                Console.Error.WriteLine("Usage: " + typeof(Client).Name + "<host> <port> <number of clients>");
            var host = args[0];
            var port = Convert.ToInt32(args[1]);
            var numberOfClients = Convert.ToInt32(args[2]);
            new Client(host, port, numberOfClients).Start().Wait();
        }
    }
}
