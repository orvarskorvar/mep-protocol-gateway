﻿using System;
using System.Text;
using System.Collections;

using DotNetty.Buffers;
using DotNetty.Transport.Channels;
using Mep.Common;
using Mep.Common.Domain;

namespace Mep.Client
{
    class ClientHandler : ChannelHandlerAdapter
    {
        public IByteBuffer message = Unpooled.CopiedBuffer(Encoding.UTF8.GetBytes("Netty Rocks!"));

        public override void ChannelActive(IChannelHandlerContext context)
        {
            // Send a handshake envelope
            var handshakePayload = new HandShakePayLoad(2, 1, 0x2f3a387b1d, 556677, 1, 4, 69017);
            var length = handshakePayload.AsByteArray().Length;
            var envelope = new Envelope(Envelope.EnvelopeType.Handshake, true, false, false, false, false, 1, 1, 2, 128, length, handshakePayload, 17070);
            var buffer = envelope.AsByteBuffer();
            Console.WriteLine("Sending handshake..." + ByteBufferUtil.PrettyHexDump(buffer));
            context.WriteAndFlushAsync(buffer);
        }

        bool IsBitSet(byte b, byte nPos)
        {
            return new BitArray(new[] { b })[nPos];
        }
        byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("a byte contains exactly 8 bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }
        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            IByteBuffer buf = (IByteBuffer)message;
            
            var envelope = new Envelope(buf);
            Console.WriteLine("Client recieved: " + envelope.Type);
            if (envelope.Type == Envelope.EnvelopeType.InternalRequest)
            {
                var response = EnvelopeHelper.InternalResponse(envelope);
                context.Channel.WriteAndFlushAsync(response);
            }
        }


        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.Error.WriteLine(exception.Message);
            context.CloseAsync().Wait();
        }
    }
     
}
