﻿using System;
using System.Text;
using NUnit.Framework;

using DotNetty.Buffers;
using DotNetty.Transport.Channels.Embedded;
using Mep.Common.Handlers;

namespace Mep.Test
{
    [TestFixture]
    public class HdlcCodecTests
    {
        [Test]
        public void TestHdclDecoder()
        {
            var input = TestUtils.GetAsBuffer("~Orvar~~Orv}Ar~~Andreas{10}]~~Fredrik}]}]}]~");

            var channel = new EmbeddedChannel(new HdlcDecoder());
            Assert.True(channel.WriteInbound(input));
            Assert.True(channel.Finish());

            //read messages
            var read = (IByteBuffer) channel.ReadInbound<object>();
            TestUtils.Logger(read);
            Assert.True(read.Equals(Unpooled.CopiedBuffer(Encoding.UTF8.GetBytes("Orvar"))));

            read = (IByteBuffer) channel.ReadInbound<object>();
            TestUtils.Logger(read);
            Assert.True(read.Equals(Unpooled.CopiedBuffer(Encoding.UTF8.GetBytes("Orvar"))));

            read = (IByteBuffer)channel.ReadInbound<object>();
            TestUtils.Logger(read);
            Assert.True(read.Equals(Unpooled.CopiedBuffer(Encoding.UTF8.GetBytes("Andreas{10}"))));

            read = (IByteBuffer)channel.ReadInbound<object>();
            TestUtils.Logger(read);
            Assert.True(read.Equals(Unpooled.CopiedBuffer(Encoding.UTF8.GetBytes("Fredrik}}}"))));
        }

        [Test]
        public void TestHdclEncoder()
        {
            var input = TestUtils.GetAsBuffer("Detta är ett helt vanligt ~~}}}~} meddelande!");
 
            var channel = new EmbeddedChannel(new HdlcEncoder());
            Assert.True(channel.WriteOutbound(input));
            Assert.True(channel.Finish());

            var read = (IByteBuffer)channel.ReadOutbound<object>();
            TestUtils.Logger(read);
            var result = TestUtils.GetAsBuffer("~Detta är ett helt vanligt }^}^}]}]}]}^}] meddelande!~");
            Assert.AreEqual(read, result);
        }

        [Test]
        public void TestHdclCodec()
        {
            var random = new Random();
            var bytes = new byte[999];
            random.NextBytes(bytes);
            var data = TestUtils.GetAsBuffer(bytes);
            var data2 = data.Duplicate();

            var channel = new EmbeddedChannel(new HdlcEncoder());
            Assert.True(channel.WriteOutbound(data.Retain()));
            Assert.True(channel.Finish());
            var encoded = (IByteBuffer)channel.ReadOutbound<object>();
            TestUtils.Logger(encoded);

            channel = new EmbeddedChannel(new HdlcDecoder());
            Assert.True(channel.WriteInbound(encoded));
            Assert.True(channel.Finish());

            var decoded = (IByteBuffer) channel.ReadInbound<object>();
            TestUtils.Logger(decoded);

            Assert.True(decoded.Equals(data2));
        }
    }
}
