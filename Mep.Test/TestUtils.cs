﻿using System;
using DotNetty.Buffers;
using Mep.Common;
using Mep.Common.Utils;

namespace Mep.Test
{
    static class TestUtils
    {
        public static void Logger(IByteBuffer frame)
        {
            Console.WriteLine(ByteBufferUtil.PrettyHexDump(frame));
        }
        public static IByteBuffer GetAsBuffer(string str)
        {
            var buf = Unpooled.Buffer();
            foreach (var b in str)
            {
                buf.WriteByte(b);
            }
            return buf;
        }

        public static byte[] GetAsByteArray(string str)
        {
            var arr = new byte[str.Length];
            int i = 0;
            foreach (var b in str)
            { 
                arr[i] = (byte) b;
                i++;
            }
            return arr;
        }

        public static IByteBuffer GetAsBuffer(byte[] bytes)
        {
            var buf = Unpooled.Buffer();
            foreach (var b in bytes)
            {
                buf.WriteByte(b);
            }
            return buf;
        }

        public static IByteBuffer AsBuffer(this byte[] bytes)
        {
            return GetAsBuffer(bytes);
        }

        public static byte[] AsByteArray(this string str)
        {
            return GetAsByteArray(str);
        }

        public static string GetBufferAsString(IByteBuffer buf)
        {
            var buffer = Unpooled.CopiedBuffer(buf);
            var result = "";
            while (buffer.IsReadable())
            {
                result += buffer.ReadByte();
            }
            return result;
        }

        public static IByteBuffer Append(this IByteBuffer buffer1, IByteBuffer buffer2)
        {
            var buf = Unpooled.Buffer();
            buf.WriteBytes(buffer1);
            buf.WriteBytes(buffer2);
            return buf;
        }

        public static IByteBuffer AddChecksum(this IByteBuffer buffer)
        {
            var crc = Crc16.ComputeChecksum(buffer);
            buffer.WriteShort(crc);
            return buffer;
        }

        public static byte[] GetRandomArray(int length)
        {
            var random = new Random();
            var bytes = new byte[length];
            random.NextBytes(bytes);
            return bytes;
        }
    }
}
