﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetty.Buffers;
using Mep.Common;
using Mep.Common.Domain;
using Mep.Common.Utils;
using NUnit.Framework;

namespace Mep.Test
{
    [TestFixture]
    internal class EnvelopeTests
    {
        readonly byte [] _payload = TestUtils.GetAsByteArray("A checksum is a small-sized datum derived from a block of digital data for the purpose of detecting errors which may have been introduced during its transmission or storage. It is usually applied to an installation file after it is received from the download server. By themselves, checksums are often used to verify data integrity but are not relied upon to verify data authenticity.");
        readonly byte[] _hpayload = TestUtils.GetAsByteArray("A chec");

        [SetUp]
        public void SetUp()
        {
           

        }

        [Test]
        public void TestCreateHandshakeEnvelope()
        {
            const byte one = 0x01;
            const byte type = 0x31;
            const byte pid = 0x1f;
            
            var handshakePayloadBuffer = TestUtils.GetAsBuffer(new byte[] { 0x82 }).Append(Vli.EncodeVariableLengthInt(0x001c42000009)).Append(Vli.EncodeVariableLengthInt(65427653)).Append(Vli.EncodeVariableLengthInt(1)).Append(Vli.EncodeVariableLengthInt(4)).Append(Vli.EncodeVariableLengthInt(612487));
            var pbuf = Unpooled.CopiedBuffer(handshakePayloadBuffer);
            var plen = Vli.EncodeVariableLengthInt((ulong)handshakePayloadBuffer.ReadableBytes);
            var buffer = TestUtils.GetAsBuffer(new []{type, one , one , one, pid});
            buffer.WriteBytes(plen);
            buffer.WriteBytes(handshakePayloadBuffer);

            var crc = Crc16.ComputeChecksum(buffer);
            buffer.WriteUnsignedShort(crc);
            var envelope = new Envelope(buffer);

            Assert.That(envelope.Type == Envelope.EnvelopeType.Handshake);
            Assert.True(envelope.TcCrc);
            Assert.True(envelope.TcAck);
            
            Assert.AreEqual(pbuf, envelope.Payload.AsByteBuffer());
            Assert.AreEqual(crc, envelope.Checksum);
        }

        [Test]
        public void TestCreateRequestEnvelope()
        {
            var payload = TestUtils.GetRandomArray(595);
            var reqEnvBuffer = TestUtils.GetAsBuffer(new byte[] { 0x12, 0x63 }).Append(Vli.EncodeVariableLengthInt(595)).Append(payload.AsBuffer());
            var crc = Crc16.ComputeChecksum(reqEnvBuffer);
            reqEnvBuffer.WriteUnsignedShort(crc);
            var reqEnv = new Envelope(reqEnvBuffer);
            Envelope.CheckEnvelope(reqEnv, new ArrayList() { Envelope.EnvelopeType.Request, true, null, null, null, null, null, null, null, null, (byte) 99, 595, payload, crc});
        }

        [Test]
        public void TestCreateResponseEnvelope()
        {
            var payload = TestUtils.GetRandomArray(618);
            var envBuffer = TestUtils.GetAsBuffer(new byte[] { 0x33, 0xDE }).Append(Vli.EncodeVariableLengthInt(618)).Append(payload.AsBuffer());
            var crc = Crc16.ComputeChecksum(envBuffer);
            envBuffer.WriteUnsignedShort(crc);
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.Response, true, null, null, null, null, null, null, null, null, (byte)222, 618, payload, crc });
        }

        [Test]
        public void TestCreatePostEnvelope()
        {
            var payload = TestUtils.GetRandomArray(614);
            var envBuffer = TestUtils.GetAsBuffer(new byte[] { 0x34, 0x7D }).Append(Vli.EncodeVariableLengthInt(614)).Append(payload.AsBuffer());
            var crc = Crc16.ComputeChecksum(envBuffer);
            envBuffer.WriteUnsignedShort(crc);
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.Post, true, null, null, null, null, null, null, null, null, (byte)125, 614, payload, crc });
        }

        [Test]
        public void TestCreateKeepaliveEnvelope()
        {
            var envBuffer = TestUtils.GetAsBuffer(new byte[] { 0x26, 0x36 });
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.Keepalive, false, true, false, false, null, null, null, null, null, (byte)54, null, null, null });
        }

        [Test]
        public void TestCreateAckEnvelope()
        {
            var envBuffer = TestUtils.GetAsBuffer(new byte[] {0x87, 0x0C});
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.Ack, false, false, false, false, true, false, null, null, null, (byte)12, null, null, null });
        }

        [Test]
        public void TestCreateInternalRequestEnvelope()
        {
            var payload = TestUtils.GetRandomArray(45);
            var envBuffer = TestUtils.GetAsBuffer(new byte[] { 0x18, 0xC6 }).Append(Vli.EncodeVariableLengthInt(45)).Append(payload.AsBuffer());
            var crc = Crc16.ComputeChecksum(envBuffer);
            envBuffer.WriteUnsignedShort(crc);
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.InternalRequest, true, null, null, null, null, null, null, null, null, (byte)198, 45, payload, crc });
        }

        [Test]
        public void TestCreateInternalResponseEnvelope()
        {
            var payload = TestUtils.GetRandomArray(300);
            var envBuffer = TestUtils.GetAsBuffer(new byte[] { 0x19, 0x4B }).Append(Vli.EncodeVariableLengthInt(300)).Append(payload.AsBuffer());
            var crc = Crc16.ComputeChecksum(envBuffer);
            envBuffer.WriteUnsignedShort(crc);
            var envelope = new Envelope(envBuffer);
            Envelope.CheckEnvelope(envelope, new ArrayList() { Envelope.EnvelopeType.InternalResponse, true, null, null, null, null, null, null, null, null, (byte)75, 300, payload, crc });
        }

        // ---------------------------------------------- Encode Envelope Tests ------------------------------------------------------

        [Test]
        public void TestEncodeHandshake()
        {
            var handshakePayload = new HandShakePayLoad();
            var length = handshakePayload.AsByteArray().Length;
            var envelope = new Envelope(Envelope.EnvelopeType.Handshake, true, false, false, false, false, 1, 1, 1, 128, length, handshakePayload, 17070);
            var buffer = envelope.AsByteBuffer();
            var env2 = new Envelope(buffer);
            Assert.True(envelope.Equals(env2));
        }
        [Test]
        public void TestEncodeRequest()
        {
            var envelope = new Envelope(Envelope.EnvelopeType.Request, true, false, false, false, false, 0, 0, 0, 255, 384, new ByteArrayPayload(_payload), 35073);
            var buffer = envelope.AsByteBuffer();
            var env2 = new Envelope(buffer);
            Assert.True(envelope.Equals(env2));
        }

        [Test]
        public void TestEncodeAck()
        {
            var envelope = new Envelope(Envelope.EnvelopeType.Ack, false, false, false, false, true, packetId:128);
            var buffer = envelope.AsByteBuffer();
            var env2 = new Envelope(buffer);
            Assert.True(envelope.Equals(env2));
        }
        [Test]
        public void TestEncodeKeepAlive()
        {
            var envelope = new Envelope(Envelope.EnvelopeType.Keepalive, false, true, false, false, false, packetId: 234);
            var buffer = envelope.AsByteBuffer();
            var env2 = new Envelope(buffer);
            Assert.True(envelope.Equals(env2));
        }

    }
}
