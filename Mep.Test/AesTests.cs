﻿using System;
using System.Linq;
using NUnit.Framework;
using Mep.Common;
using Mep.Common.Utils;

namespace Mep.Test
{
    [TestFixture]
    class AesTests
    {
        [Test]
        public void TestAesCrypto()
        {
            var plaintext = "This is the secret message!".AsByteArray();
            var encrypted = AesCrypto.Encrypt(plaintext);
            
            var decrypted = AesCrypto.Decrypt(encrypted);
            Assert.AreEqual(plaintext, decrypted);
        }
        [Test]
        public void TestAesCrypto2()
        {
            // spec is to only cipher one block = 16 bytes
            var plaintext = TestUtils.GetRandomArray(16);
            var encrypted = AesCrypto.Encrypt(plaintext);

            var decrypted = AesCrypto.Decrypt(encrypted);
            Assert.AreEqual(plaintext, decrypted);
        }

        [Test]
        public void TestAesCrypto3()
        { 
            var plaintext = TestUtils.GetRandomArray(16);
            var encrypted = AesCrypto.Encrypt(plaintext);

            Assert.That(encrypted.Length == 32);

            var decrypted = AesCrypto.Decrypt(encrypted);
            Assert.That(decrypted.Length == 16);
        }
    }
}
