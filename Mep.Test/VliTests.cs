﻿using DotNetty.Buffers;
using NUnit.Framework;
using Mep.Common;
using Mep.Common.Utils;

namespace Mep.Test
{
    [TestFixture]
    class VliTests
    {
        [Test]
        public void TestToVliBufferZero()
        {
            ulong SoftwareId = 0;
            var buffer = SoftwareId.ToVliBuffer();
            Assert.That(buffer.ReadableBytes == 1);
            Assert.That(buffer.ReadByte() == 0);
        }

        [Test]
        public void ZeroVliBufferToInt()
        {
            var buf = Unpooled.Buffer();
            buf.WriteByte(0);
            ulong softwarId = buf.VliBufferToInt();
            Assert.That(buf.ReadableBytes == 0);
            Assert.That(softwarId == 0);
        }

        [Test]
        public void TestVliEncodeDecode()
        {
            ulong[] testNumbers = {0, 2097152, 2097151, 1, 127, 128, 589723405834L, 0};
            foreach (var n in testNumbers)
            {
                var encoded = n.ToVliBuffer();
                var decoded = encoded.VliBufferToInt();
                Assert.AreEqual(n, decoded);
            }
        }

        [Test]
        public void TestVliMultipleEncodeDecode()
        {
            ulong[] testNumbers = {0, 2097152, 2097151, 1, 127, 128, 589723405834L, 0};

            var buffer = Unpooled.Buffer();
            foreach (var n in testNumbers)
            {
                buffer.WriteBytes(n.ToVliBuffer());
            }
            ulong decoded = 0;
            foreach (var n in testNumbers)
            {
                decoded = buffer.VliBufferToInt();
                Assert.AreEqual(n, decoded);
            }
        }

        [Test]
        public void TestSpecificVarInt()
        {
            byte [] test = new byte[] {0xc1, 0x01, 0x3c, 0x3f, 0x78};
            var plen = (int)Vli.DecodeVariableLengthInt(test.AsBuffer());
            Assert.AreEqual(plen, 8321);

        }

        [Test]
        public void TestSpecificVarInt2()
        {
            byte[] test = new byte[] { 0xa0, 0x01 };
            var plen = (int)Vli.DecodeVarInt(test.AsBuffer());
            Assert.AreEqual(plen, 160);

        }

        [Test]
        public void TestNewestVliEncodeDecodeVarInt()
        {
            int[] testNumbers = { 0, 160, 2097152, 2097151, 1, 127, 128, 12345, 0 };
            foreach (var n in testNumbers)
            {
                var encoded = Vli.EncodeVarInt(n);
                var decoded = Vli.DecodeVarInt(encoded);
                Assert.AreEqual(n, decoded);
            }
        }

        [Test]
        public void TestNewestVliEncodeDecodeVarLong()
        {
            long[] testNumbers = { 0, 160, 1625738949876, 2097151, 1, 127, 128, 12345, 0 };
            foreach (var n in testNumbers)
            {
                var encoded = Vli.EncodeVarLong(n);
                var decoded = Vli.DecodeVarLong(encoded);
                Assert.AreEqual(n, decoded);
            }
        }
    }
}
