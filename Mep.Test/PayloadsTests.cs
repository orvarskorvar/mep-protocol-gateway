﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mep.Common;
using Mep.Common.Domain;
using Mep.Common.Utils;
using NUnit.Framework;

namespace Mep.Test
{
    [TestFixture]
    class PayloadsTests
    {


        // ------------------------------------ HandShakePayLoad Tests-------------------------------------------------------------------
        [Test]
        public void TestCreateHandshakePayload()
        {
            var handshakePayloadBuffer = TestUtils.GetAsBuffer(new byte[] { 0x82 }).Append(Vli.EncodeVariableLengthInt(0x001c42000009)).Append(Vli.EncodeVariableLengthInt(65427653)).Append(Vli.EncodeVariableLengthInt(1)).Append(Vli.EncodeVariableLengthInt(4)).Append(Vli.EncodeVariableLengthInt(612487));
            var hp = new HandShakePayLoad(handshakePayloadBuffer);
            Assert.That(hp.CTypeId == HandShakePayLoad.ConnectionIdType.Mac);
            Assert.That(hp.CType == HandShakePayLoad.ConnectionType.EthernetWifi);
            Assert.That(hp.ConnectionId.Value() == 0x001c42000009);
            Assert.That(hp.SoftwareId.Value() == 65427653);
            Assert.That(hp.MajorVersion.Value() == 1);
            Assert.That(hp.MinorVersion.Value() == 4);
            Assert.That(hp.BuildNumber.Value() == 612487);
        }


        [Test]
        public void TestEncodeHandshakePayload()
        {
            var handshakePayload = new HandShakePayLoad(2, 1, 0x2f3a387b1d, 556677, 0, 4, 69017);
            var buffer = handshakePayload.AsByteBuffer();
            var hp2 = new HandShakePayLoad(buffer);
            Assert.True(handshakePayload.Equals(hp2));
        }

        [Test]
        public void TestCreateInternalRequestPayload()
        {
            
            int unixTime = (int)((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds();
            var intReqPayload = new InternalRequestPayload(new VarLong(1234567), new VarInt(unixTime), new VarInt(1), new VarInt(1), new VarInt(4));
        }
    }
}
