﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mep.Common.Domain;
using NUnit.Framework;

namespace Mep.Test
{
    [TestFixture]
    class SpMessageTest
    {
        [Test]
        public void ToXmlTest1()
        {
            SpMessage message = new SpMessage(SpMessage.SpMessageType.result,"accepted");
            Console.WriteLine(message.ToXml());

            List<SpMessage.Reading.R> rlist = new List<SpMessage.Reading.R>
            {
                new SpMessage.Reading.R(2, 5, 1379232000),
                new SpMessage.Reading.R(3, 5),
                new SpMessage.Reading.R(2, 5)
            };
            var readings = new SpMessage.Reading(rlist);
            message = new SpMessage(SpMessage.SpMessageType.reading, r:readings);
            Console.WriteLine(message.ToXml());
        }

        [Test]
        public void XmlToMessageToxmlTests()
        {
            string[] xmlStrings =
            {
                @"<?xml version=""1.0"" encoding=""UTF-8""?><message type=""result"" status=""accepted""></message>".ToLower(),
                @"<?xml version=""1.0"" encoding=""UTF-8""?><message type=""result"" status=""accepted""></message>".ToLower(),
                @"<?xml version=""1.0"" encoding=""UTF-8""?><message type=""result"" status=""accepted""></message>".ToLower()
            };
            foreach (var xml in xmlStrings)
            {
                SpMessage message = new SpMessage(xml);
                Assert.AreEqual(xml, message.ToXml());
            }
        }

    }
}
