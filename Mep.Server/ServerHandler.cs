﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using DotNetty.Common.Utilities;
using Mep.Common.Utils;
using Mep.Common.Domain;

namespace Mep.Server
{
    using System;
    using DotNetty.Buffers;
    using DotNetty.Transport.Channels;

    public class ServerHandler : ChannelHandlerAdapter
    {
        public IByteBuffer Message;

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {

            string value = context.GetAttribute(Server.id).Get();
            Console.WriteLine(value);
            
            var buffer = message as IByteBuffer;
            if (buffer != null)
            {
                var envelope = new Envelope(buffer);
                Console.WriteLine("Received from client: " + envelope.Type);
                switch (envelope.Type)
                {
                    case Envelope.EnvelopeType.Handshake:
                        if (envelope.HType == Envelope.HandshakeType.AesChallenge)
                        {
                            // make an internal request to respond with..
                            var intReq = EnvelopeHelper.InternalRequest(envelope);
                            Message = intReq.AsByteBuffer();
                            context.WriteAndFlushAsync(Message);

                        } 

                        // Authenticate with iot-hub
                        break;
                    case Envelope.EnvelopeType.InternalResponse:
                        // Handshake is complete.
                        break;
                    case Envelope.EnvelopeType.Request:
                        // send response
                        var mess =
                            @"<?xml version=""1.0"" encoding=""UTF-8""?><message type=""result"" status=""accepted""></message>";
                        context.WriteAndFlushAsync(EnvelopeHelper.Response(envelope.PacketId, true, mess).AsByteBuffer());
                        break;
                    case Envelope.EnvelopeType.Response:
                        break;
                    case Envelope.EnvelopeType.Post:
                        break;
                    case Envelope.EnvelopeType.Keepalive:
                        break;
                    case Envelope.EnvelopeType.Ack:
                        break;
                    case Envelope.EnvelopeType.InternalRequest:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                
            }  
        }


        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.WriteLine("Exception: " + exception);
            context.CloseAsync();
        }
    }
}