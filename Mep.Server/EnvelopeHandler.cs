﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using System.Threading.Tasks;
using DotNetty.Common.Utilities;
using Mep.Common.Domain;

namespace Mep.Server
{
    using System;
    using DotNetty.Buffers;
    using DotNetty.Transport.Channels;

    public class EnvelopeHandler : ChannelHandlerAdapter
    {
        public IByteBuffer Message;

       

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            //context.Channel.GetAttribute(id).Get() = 3;
            context.GetAttribute(Server.id).Set("ORVARSDATA");
            Console.WriteLine("EnvelopeHandler READ");
            context.FireChannelRead(message);
        }

        public override Task WriteAsync(IChannelHandlerContext ctx, object msg)
        {
            Console.WriteLine("EnvelopeHandler WRITE");
            return ctx.WriteAsync(msg);
        }


        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Console.WriteLine("Exception: " + exception);
            context.CloseAsync();
        }
    }
}