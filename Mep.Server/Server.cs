﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Logging.Console;

using DotNetty.Handlers.Tls;
using DotNetty.Common.Internal.Logging;
using DotNetty.Common.Utilities;
using DotNetty.Handlers.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Mep.Common.Handlers;


namespace Mep.Server
{
    public class Server
    {
        public static readonly AttributeKey<String> id = AttributeKey<String>.NewInstance("ID");
        private readonly int port;
        
        public Server(int port)
        {
            this.port = port;
        }
 
        static void Main(string[] args)
        {
            Console.WriteLine("STARTING SERVER");
            if (args.Length != 1)
            {
                Console.Error.WriteLine("Usage: " + typeof(Server).Name + " <port>");
            }
            int port = Convert.ToInt32(args[0]);
            new Server(port).start().Wait();
        }

        public async Task start()
        {
            InternalLoggerFactory.DefaultFactory.AddProvider(new ConsoleLoggerProvider((s, level) => true, false));
            IEventLoopGroup group = new MultithreadEventLoopGroup();
            var bossGroup = new MultithreadEventLoopGroup(1);
            var workerGroup = new MultithreadEventLoopGroup();
            X509Certificate2 certificate = new X509Certificate2(Path.Combine(AppContext.BaseDirectory, "mep-server.pfx"), "!password");
            var hdlcDecoder = new HdlcDecoder();
            try
            {
                ServerBootstrap b = new ServerBootstrap();
                b.Group(bossGroup, workerGroup)
                    .Channel<TcpServerSocketChannel>()
                    .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                    {
                        IChannelPipeline pipeline = channel.Pipeline;
                        pipeline.AddLast("ssl", TlsHandler.Server(certificate));
                        pipeline.AddLast(new LoggingHandler());
                        pipeline.AddLast("hdlc-dec", hdlcDecoder);
                        pipeline.AddLast("hdlc-enc", new HdlcEncoder());
                        pipeline.AddLast("env-handler", new EnvelopeHandler());
                        pipeline.AddLast("mep-server", new ServerHandler() );
                    }));


                IChannel boundChannel = await b.BindAsync(port);
                Console.WriteLine("Listening on port: " + port + " press any key to quit");
                Console.ReadKey(false);
                Console.WriteLine("Closing server...");
                await boundChannel.CloseAsync();

            }
            finally
            {
                await Task.WhenAll(
                    bossGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(100), TimeSpan.FromSeconds(1)),
                    workerGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(100), TimeSpan.FromSeconds(1)));
            }

        }

    }
}
